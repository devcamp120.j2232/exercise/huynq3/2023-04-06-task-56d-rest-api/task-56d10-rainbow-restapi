package com.devcamp.rainbowrequestinputapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRequestInputApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRequestInputApiApplication.class, args);
	}

}
